/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

/**
 *
 * @author juanluis
 */


public class BTree<Template> {
    //llaves
    private Template[] keys;
    private int load;
    //cardinalidad numero de valores por nodo = grade-1
    private int grade;
    //apuntador padre
    private BTree father;
    private BTree[] children;

    private boolean hasChildren(){
        for(int i=0;i<grade;i++)
            return (children[0] != null);
        return false;
    }

    public BTree(int grade){
        load=0;
        this.grade=grade;
        //se le da una casilla extra al array de llaves y de hijos para poder guardar temporalmente el valor antes de separar
        keys=(Template[])new Object[grade];
        children=new BTree[grade+1];
        father=null;
    }

    public BTree<Template> insert(Template val){
        //si es un nodo sin hijos se inserta aqui

        if(! hasChildren())
            
            addSorted(val,null,null);
        else {
            ///se debe determinar en que apuntador hijo se debe insertar el nuevo valor

            findPositionOfValue(val).insert(val);

        }
        return this;

    }

    private BTree<Template> findPositionOfValue(Template val){
        int position=load;
        for (int i = 0; i < load; i++)
            if (val.hashCode() < keys[i].hashCode()) {
                position = i;
                break;
            }
        return children[position];
    }

    public String print() {
        String sout="no. llaves "+load+"\n";
        sout+="[";
        for(int i=0;i<load;i++)
            sout+=keys[i]+", ";
        sout+="]\n";
        for(int i=0;i<=load;i++)
            if(children[i] != null) {
                sout+= "hijo"+i+" de "+keys[0]+"\n";
                sout+=children[i].print();
            }
        return sout;
    }

//metodo recursivo
    private void addSorted(Template val,BTree pleft,BTree pright) {
        //se recorre de atras para adelante porque los numeros se van llenando del indece 0,1..load-1
        //se tiene una bandera para saber si se inserto el valor al terminar el ciclo
        boolean isInserted=false;
        if(val.hashCode()==17){
            System.out.println("inserta 20");
        }
        for(int i=load-1;i>=0;i--)
            //si el valor es menor quiere decir que se debe correr el valor actual a la siguiente posicion del array
            if(val.hashCode() < keys[i].hashCode()) {
                keys[i + 1] = keys[i];
                //tambien se acarrean los punteros
                children[i+2] = children[i+1];
            }
            else {
                keys[i + 1] = val;
                children[i+1] = pleft;
                children[i+2] = pright;
                isInserted=true;
                break;
            }
         if(!isInserted) {
             keys[0] = val;
             children[0]=pleft;
             children[1]=pright;
         }
         load++;
        //si la carga es menor a la cantidad maxima de llaves permitidas no se hace la siguiente parte de codigo
         //se hace la separacion
         if(load == grade){
             // se calcula posicion de valor medio para separar
             int med = (grade-1)/2;
             //se crea arbol izquierdo
             BTree left=new BTree(grade);

             for(int i=0;i<med;i++) {
                 left.keys[i] = keys[i];
                 left.children[i]=children[i];
                 if(left.children[i]!=null){
                     left.children[i].father=left;
                 }
             }
             //se asigna el hijo de en medio
             left.children[med]=children[med];
             if(left.children[med]!=null)
                 left.children[med].father=left;
             left.setLoad(med);
             //se crea arbol derecho
             BTree right=new BTree(grade);

             for(int i=med+1;i<grade;i++) {
                 right.keys[i - med - 1] = keys[i];
                 right.children[i-med-1] =children[i];
                 if(right.children[i-med-1]!=null){
                     right.children[i-med-1].father=right;
                 }
             }
             right.children[grade-med-1] =children[grade];
             if(right.children[grade-med-1]!=null)
                 right.children[grade-med-1].father=right;
             right.setLoad(grade-med-1);

             //si el padre es null el nodo actual  se le pondra el valor medio y se le colocan como hijos los dos nuevos nodos
             // ,sino solo se agrega el valor medio al padre

             if(this.father==null) {
                 Template[] keys1=(Template[])new Object[grade];
                 keys1[0]=keys[med];
                 keys=keys1;
                 load=1;
                 children[0]=left;
                 children[1]=right;
                 for(int i=2;i<grade;i++)
                     children[i]=null;
                 left.father=this;
                 right.father=this;
             } else {
                 left.father=this.father;
                 right.father=this.father;
                 father.addSorted(keys[med],left,right);
             }

         }
    }

    public BTree<Template> delete(Template val){
        boolean deleted=false;
        for(int i=0;i<load;i++)
            if(keys[i].hashCode()==val.hashCode()) {
                deleted=true;
                deleteKey(val, i);
            }
        if(!deleted  && hasChildren())
            findPositionOfValue(val).delete(val);
        return this;
    }

    private void deleteKey(Template val,int pos) {
        for(int i=pos;i<load;i++)
            keys[i]=keys[i+1];
        load--;
        //if(load < (grade-1)/2)


    }

    public Template[] getKeys() {
        return keys;
    }

    public void setKeys(Template[] keys) {
        this.keys = keys;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public BTree getFather() {
        return father;
    }

    public void setFather(BTree father) {
        this.father = father;
    }

    public BTree[] getChildren() {
        return children;
    }

    public void setChildren(BTree[] children) {
        this.children = children;
    }

//    public static void main(String[] arg){
//         ///Timer timer=new Timer();
//        BTree<Integer> arbolB=new BTree<>(5);
//
//       //timer.start();
//
//        for(int i=1;i<=5;i++)
//            arbolB.insert(i);
//
//
//        System.out.println("impresion de datos");
//        arbolB.print();
//
//
//    }
}
